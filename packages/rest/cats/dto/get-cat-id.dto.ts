import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsInt } from 'class-validator';

export class getByIdDto {
  @ApiModelProperty()
  @IsInt()
  readonly id: number;
}