import { Controller, Param, Body, UseGuards, HttpCode, UseInterceptors, HttpStatus } from '@nestjs/common';
import { Req, Res, Next } from '@nestjs/common';
import { Get, Post, Put, Delete } from '@nestjs/common';
import { ValidationPipe, ParseUUIDPipe, ParseIntPipe, UsePipes} from '@nestjs/common';
import { Request, Response } from 'express';

import { CreateCatDto } from './dto/create-cat.dto';
import { UpdateCatDto } from './dto/update-cat.dto';
import { Cat } from './classes/cat.class';

import { CatsService } from '../../service/cats/cats.service';
import { ApiUseTags, ApiCreatedResponse, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { LoggingInterceptor } from '../interceptors/logging.interceptor';
import { TimeoutInterceptor } from '../interceptors/timeout.interceptor';

@Controller('cats')
@ApiUseTags('cats')
@UseInterceptors(LoggingInterceptor)
export class CatsController {
  constructor(private readonly catsService: CatsService) { }

  @Post()
  @ApiOperation({ title: 'Create cat' })
  @ApiResponse({
    status: 201,
    description: 'New cat successfully added',
    type: Cat
  })
  @UsePipes(new ValidationPipe({transform: true}))
  async create(@Body() creatCatDto: CreateCatDto): Promise<Cat> {
    return await this.catsService.create(creatCatDto);
  }

  @Get()
  @UseInterceptors(TimeoutInterceptor)
  @ApiResponse({
    status: 200,
    type: [Cat]
  })
  async findAll(): Promise<Cat[]> {
    return await this.catsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', new ParseIntPipe()) id: number): Promise<Cat> {
    return await this.catsService.findOne(id);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe({transform: true}))
  async update(@Param('id', new ParseIntPipe()) id: number, @Body() updateCatDto: UpdateCatDto): Promise<Cat> {
    return await this.catsService.update(id, updateCatDto);
  }

  @Delete(':id')
  async remove(@Param('id', new ParseIntPipe()) id: number): Promise<any> {
    return await this.catsService.remove(id);
  }
}
