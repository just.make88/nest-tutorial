import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-google-oauth20";
import { AuthService, Provider } from "../../../service/auth/auth.service";
import { clientKeys } from '../constants/constants';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      clientID: clientKeys.clientID,
      clientSecret: clientKeys.clientSecret,
      callbackURL: clientKeys.callbackURL,
      passReqToCallback: true,
      scope: ['profile']
    })
  }

  async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function) {
    try {
      const jwt: string = await this.authService.validateOAuthLogin(profile.id, Provider.GOOGLE);
      const user = { jwt };

      done(null, user)
    }
    catch (error) {
      done(error, false);
    }
  }
}