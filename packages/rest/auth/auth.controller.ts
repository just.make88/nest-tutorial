import { Controller, Get, UseGuards, Req, Res, Next, Post, Request, Body, Query, UseInterceptors, Header } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LoginUserDto } from '../users/dto/login-user.dto';
import { AuthService } from '../../service/auth/auth.service';
import { ApiUseTags, ApiOAuth2Auth, ApiBearerAuth } from '@nestjs/swagger';
import { LoggingInterceptor } from '../interceptors/logging.interceptor';

@Controller('auth')
@ApiUseTags('auth')
@UseInterceptors(LoggingInterceptor)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOAuth2Auth(['profile'])
  @ApiBearerAuth()
  @UseGuards(AuthGuard('google'))
  @Get('googleLogin')
  googleLogin() {
    console.log('123')
    // res.redirect(clientKeys.callbackURL)
  }

  @ApiOAuth2Auth(['profile'])
  @ApiBearerAuth()
  @UseGuards(AuthGuard('google'))
  @Get('google/callback')
  googleLoginCb(@Req() req, @Res() res) {
    const jwt: string = req.user.jwt;

    if(jwt) {
      res.send('login success');
    } else {
      res.send('login failed');
    }
  }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req, @Body() loginUserDto: LoginUserDto) {
    return this.authService.login(loginUserDto);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('getUserInfo')
  getProfile(@Request() req) {
    return req.user;
  }
}
