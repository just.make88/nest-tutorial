import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsInt } from 'class-validator';

export class LoginGoogleDto {
  // @ApiModelProperty()
  // @IsInt()
  // readonly userId: number;

  @ApiModelProperty()
  @IsString()
  readonly email: string;
  
  @ApiModelProperty()
  @IsString()
  readonly password: string;
}