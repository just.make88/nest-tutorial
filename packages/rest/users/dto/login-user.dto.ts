import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsInt } from 'class-validator';

export class LoginUserDto {
  // @ApiModelProperty()
  // @IsInt()
  // readonly userId: number;

  @ApiModelProperty()
  @IsString()
  readonly username: string;
  
  @ApiModelProperty()
  @IsString()
  readonly password: string;
}