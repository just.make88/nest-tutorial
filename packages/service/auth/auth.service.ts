import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { sign } from 'jsonwebtoken';
import { JWT_SECRET_KEY } from '../../rest/auth/constants/constants'

export enum Provider {
  GOOGLE = 'google'
};

@Injectable()
export class AuthService {
  private readonly JWT_SECRET_KEY = JWT_SECRET_KEY;

  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService
  ) { }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };

    return {
      access_token: this.jwtService.sign(payload)
    }
  }

  async logout(user:any) {
    console.log('user', user)
  }

  async validateOAuthLogin(thirdPartyId: string, provider: Provider) {
    try {
      const payload = {
        thirdPartyId,
        provider
      };

      const jwt: string = sign(payload, this.JWT_SECRET_KEY, {expiresIn: 3600});
      
      return jwt;
    } 
    catch (err) {
      throw new InternalServerErrorException('validateOAuthLogin', err.message);
    }
  }
}
