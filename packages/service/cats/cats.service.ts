import { Injectable } from '@nestjs/common';
import { Cat } from '../../rest/cats/classes/cat.class';
import { Datastore } from '@google-cloud/datastore';

@Injectable()
export class CatsService {
  private kind = 'Cats';
  // private projectId = 'datastore-tutorial-251020';
  private projectId = 'nest-test-deploy';
  private datastore = new Datastore({
    projectId: this.projectId
  });

  create(cat) {
    const key = this.datastore.key(this.kind);
    const entity = {
      key: key,
      data: cat
    };

    return new Promise<Cat>((resolve, reject) => {
      this.datastore.save(entity, (err) => {
        if(err) return reject(err);
        return resolve(entity.data);
      })
    })
  }

  findAll() {
    const query = this.datastore.createQuery(this.kind);
    
    return new Promise<Cat[]>((resolve, reject) => {
      this.datastore.runQuery(query, (err, entities, info) => {
        if(err) return reject(err);
        return resolve(entities);
        // console.log('info', info)
      })
    })
  }

  findOne(id) {
    const key = this.datastore.key([this.kind, id]);

    return new Promise<Cat>((resolve, reject) => {
      this.datastore.get(key, (err, entity) => {
        if(err) return reject(err);
        return resolve(entity)
      })
    })
  }

  update(id, body) {
    const key = this.datastore.key([this.kind, id]);
    const entity = {
      key: key,
      data: body
    }

    return new Promise<Cat>((resolve, reject) => {
      this.datastore.save(entity, (err) => {
        if(err) return reject(err);
        return resolve(entity.data)
      })
    })
  }

  remove(id) {
    const key = this.datastore.key([this.kind, id]);
    
    return new Promise((resolve, reject) => {
      this.datastore.delete(key, (err, entity) => {
        if(err) return reject(err);
        return resolve(entity)
      })
    })
  }
}
