import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder, ApiBearerAuth } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as agent from '@google-cloud/debug-agent';

import { AppModule } from './app.module';
import { CatsModule } from './packages/rest/cats/cats.module';
import { AuthModule } from './packages/rest/auth/auth.module';

import * as helmet from 'helmet';
import { LoggingInterceptor } from './packages/rest/interceptors/logging.interceptor';
import { TimeoutInterceptor } from './packages/rest/interceptors/timeout.interceptor';
import { clientKeys } from './packages/rest/auth/constants/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new LoggingInterceptor())
  app.useGlobalInterceptors(new TimeoutInterceptor())
  app.enableCors();
  app.use(helmet());

  const options = new DocumentBuilder()
    .setTitle('NestJS tutorial')
    .setDescription('TestApp API description')
    .setVersion('1.0')
    .addOAuth2('implicit', 'http://localhost:3000/auth/googleLogin', clientKeys.callbackURL, {scope: 'profile'})
    .build()
  
  const document = SwaggerModule.createDocument(app, options, {
    include: [
      AuthModule,
      CatsModule
    ]
  });
  SwaggerModule.setup('api', app, document);
  
  const port = process.env.PORT || 3000;

  await agent.start();
  await app.listen(port);
}
bootstrap();
