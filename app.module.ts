import { Module, ValidationPipe } from '@nestjs/common';
import { APP_PIPE, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { CatsModule } from './packages/rest/cats/cats.module';
import { AuthModule } from './packages/rest/auth/auth.module';
import { UsersModule } from './packages/rest/users/users.module';
import { LoggingInterceptor } from './packages/rest/interceptors/logging.interceptor';
import { TimeoutInterceptor } from './packages/rest/interceptors/timeout.interceptor';

@Module({
  imports: [CatsModule, AuthModule, UsersModule],
  controllers: [],
  providers: [
    {
      provide: APP_PIPE,
      useClass: ValidationPipe
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor
    }
    // {
    //   provide: APP_GUARD,
    //   useClass: AuthGuard
    // }
  ],
})
export class AppModule {}
